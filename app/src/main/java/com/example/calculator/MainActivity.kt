package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var firstVariable: Double = 0.0
    private var secondVariable: Double = 0.0
    private var operation = ""
    private var result: Double = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)


        buttondot.setOnClickListener {
            if (resultTextView.text.isNotEmpty() && "." !in resultTextView.text.toString()) {
                resultTextView.text = resultTextView.text.toString() + "."
            }
        }
        backspaceButton.setOnLongClickListener {
            if (resultTextView.text.isNotEmpty())
                resultTextView.text = ""
            true
        }

    }

    fun equal(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty() && operation.isNotEmpty()) {
            secondVariable = value.toDouble()
            var result: Double = 0.0

            if (operation == ":" && secondVariable != 0.0) {
                result = firstVariable / secondVariable
                resultTextView.text = result.toString()

            } else if (operation == "*") {
                result = firstVariable * secondVariable
                resultTextView.text = result.toString()

            } else if (operation == "-") {
                result = firstVariable - secondVariable
                resultTextView.text = result.toString()

            } else if (operation == ":" && secondVariable == 0.0) {
                Toast.makeText(this, "Number can not be divided by zero", Toast.LENGTH_SHORT).show()
                resultTextView.text = ""


            } else {
                result = firstVariable + secondVariable
                resultTextView.text = result.toString()
            }
        }

        operation = ""
        firstVariable = 0.0
        secondVariable = 0.0
    }


    fun divide(view: View) {
        var value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "/"
            resultTextView.text = ""
        }

    }

    fun multiple(view: View) {
        var value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "X"
            resultTextView.text = ""
        }

    }

    fun minus(view: View) {
        var value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "-"
            resultTextView.text = ""
        }

    }

    fun plus(view: View) {
        var value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "+"
            resultTextView.text = ""
        }

    }


    fun backspace(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
            resultTextView.text = value.substring(0, value.length - 1)
    }


    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()
    }
}